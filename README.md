# squote implemented in R

A semantic search engine that takes some input text and returns some (questionably) relevant (questionably) famous quotes.

This is a reimplementation in R of an idea orignally published by Chris Wallace in the Cloudera Fast Forward Labs 
newsletter. Chris' original implementation was done in Python. I wanted to see how it would look implemented in R.
The original implementation can be found at https://github.com/cjwallace/squote.

I used a different service to calculate the Bert embeddings. Since I am currently working on creating an R package
to use the excellent [John Snow Labs Spark NLP](https://www.johnsnowlabs.com/spark-nlp) library I decided use this.

Built with:
- [John Snow Labs Spark NLP](https://www.johnsnowlabs.com/spark-nlp)
- [faiss](https://github.com/facebookresearch/faiss)
- [Shiny web app R package](https://shiny.rstudio.com/)

Quotes from [https://thewebminer.com/](https://thewebminer.com/).

## setup

First, install the necessary dependencies:
- R Spark NLP package (sparknlp)
```r
remotes::install_github("r-spark/sparknlp")
```
- Python faiss library. Follow instructions at https://github.com/facebookresearch/faiss/blob/master/INSTALL.md
- Other R packages: sparklyr, reticulate, futile.logger, glue, readr, jsonlite, shiny, plumber, dplyr
```r
install.packages(c("sparklyr", "reticulate", "futile.logger", "glue", "readr", "jsonlite", "shiny", "plumber", "dplyr"))
```

Makefile is provided to make things nice and easy.

```bash
make dirs
make data   # downloads the raw quote data
```

## running

Before we can run the app, we need embeddings of the quotes.
To generate the embeddings and save them in a JSON file, run the commands below.
This will take 15 to 30 minutes on CPU.

```bash
make embed  # this computes the embeddings
```

Once the embeddings exist, we can run the Shiny app with:

```bash
make serve  # this runs the Bert embedding API
make app # this runs the Shiny app UI
```

this will take a couple of minutes to start while it reads the quote embeddings and creates the faiss index.

Have fun!



