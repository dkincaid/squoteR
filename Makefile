.PHONY: dirs data serve embed app

dirs:
	mkdir data

data:
	curl https://thewebminer.com/d/quotes_all.csv -o data/quotes_all.csv

serve:
	Rscript BertEmbeddingService/serve.R

embed:
	Rscript scripts/embed.R

app:
	R -e "shiny::runApp('SQuoteR/')"